<?php

/**
 * @file
 * audiosoxfield widget hooks and callbacks.
 */

/**
 * Implementation of CCK's hook_widget_settings($op = 'form').
 */
function audiosoxfield_widget_settings_form($widget) {
  $form = module_invoke('filefield', 'widget_settings', 'form', $widget);

  $video_types=_audiosoxfield_supported_filetypes();
  $form['file_extensions'] = array(
          '#type' => 'textfield',
          '#title' => t('Permitted upload file extensions'),
          '#default_value' => is_string($widget['file_extensions']) ? $widget['file_extensions'] :$video_types,
          '#size' => 64,
          '#description' => t('Extensions a user can upload to this field. Separate extensions with a space and do not include the leading dot. Leaving this blank will allow users to upload a file with any extension.'),
          '#weight' => 1,
  );

  return $form;
}



/**
 * Implementation of CCK's hook_widget_settings($op = 'save').
 */
function audiosoxfield_widget_settings_save($widget) {
  $filefield_settings = module_invoke('filefield', 'widget_settings', 'save', $widget);

  return $filefield_settings;
}

/**
 * Element #value_callback function.
 */
function audiosoxfield_widget_value($element, $edit = FALSE) {
  $field = filefield_widget_value($element, $edit);

  return $field;
}

/**
 * Element #process callback function.
 */
function audiosoxfield_widget_process($element, $edit, &$form_state, $form) {
  $file = $element['#value'];
  $field = content_fields($element['#field_name'], $element['#type_name']);
  // Adding and deleting from the rendering table
  if($form_state['submitted'] == 1) {
    switch($form_state['values']['op']) {
      case t('Save'):
        if(!empty($form_state['values']['nid'])) {
          // update
          audiosox_save_file('update', $element);
        }
        // insert
        else {
          audiosox_save_file('insert', $element);
        }
        break;
      case t('Preview'):
      //        module_invoke_all('v_preview');
        break;
      case t('Delete'):
        $file = $element['#value'];
        $fid = $file['fid'];
        db_query('DELETE FROM {audio_rendering} WHERE fid = %d', $fid);
        break;
    }
  }
  return $element;
}

/**
 *
 * @param <type> $element
 * @param <type> $op 
 */
function audiosox_save_file($op, &$element) {
  $file = $element['#value'];
  $fid = $file['fid'];

  switch($op) {
    case 'insert':
      db_query('INSERT INTO {audio_rendering} (fid, pid, status, started, completed)
          VALUES (%d, %d, %d, %d, %d)',
              $fid, 0, AUDIO_RENDERING_PENDING, 0, 0);
      drupal_set_message(t('Audio submission queued for processing. Please wait: our servers are preparing your audio for web displaying.'));
      break;

    case 'update':
      $old_fid = $element['#default_value']['fid'];
      $serialized_data ['old_file'] = $old_fid;
      if($old_fid != $fid) {
        db_query('DELETE FROM {audio_rendering} WHERE fid = %d', $old_fid);
        db_query('INSERT INTO {audio_rendering} (fid, pid, status, started, completed, serialized_data)
          VALUES (%d, %d, %d, %d, %d, \'%s\')',
                $fid, 0, AUDIO_RENDERING_PENDING, 0, 0, serialize($serialized_data));
        drupal_set_message(t('Audio submission queued for processing. Please wait: our servers are preparing your audio for web displaying.'));
      }
      break;
  }
}


/**
 * FormAPI theme function. Theme the output of an image field.
 */
function theme_audiosoxfield_widget($element) {

  $z=theme('form_element', $element, $element['#children']);

  return $z;
}

function theme_audiosoxfield_widget_preview($item) {
  // Remove the current description so that we get the filename as the link.
  if (isset($item['data']['description'])) {
    unset($item['data']['description']);
  }

  return '<div class="audiosoxfield-file-info">'.
          '<div class="filename">'. theme('audiosoxfield_file', $item) .'</div>'.
          '<div class="filesize">'. format_size($item['filesize']) .'</div>'.
          '<div class="filemime">'. $item['filemime'] .'</div>'.
          '</div>';
}

function theme_audiosoxfield_widget_item($element) {
  return theme('filefield_widget_item', $element);
}

/**
 * Custom theme function for audiosoxfield upload elements.
 *
 * This function allows us to put the "Upload" button immediately after the
 * file upload field by respecting the #field_suffix property.
 */
function theme_audiosoxfield_widget_file($element) {
  $output = '';

  $output .= '<div class="audiosoxfield-upload clear-block">';

  if (isset($element['#field_prefix'])) {
    $output .= $element['#field_prefix'];
  }

  _form_set_class($element, array('form-file'));
  $output .= '<input type="file" name="'. $element['#name'] .'"'. ($element['#attributes'] ? ' '. drupal_attributes($element['#attributes']) : '') .' id="'. $element['#id'] .'" size="'. $element['#size'] ."\" />\n";

  if (isset($element['#field_suffix'])) {
    $output .= $element['#field_suffix'];
  }

  $output .= '</div>';

  return theme('form_element', $element, $output);
}

function _audiosoxfield_supported_filetypes() {

  $video_types = array(
          'mp3', 'wma', 'wav', 'wave', 'ogg'
  );
  $formatted_video_types=implode(' ', $video_types);
  return $formatted_video_types;
}