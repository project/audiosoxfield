<?php

/**
 * Get the object for the suitable player for the parameter resource
 */
function _video_field_common_get_player($element) {
  $op = _video_field_get_filetype($element['#item']['filename']);

  switch ($op) {
    case 'mp3':
      return theme('audiosoxfield_play_mp3', $element);
    default:
      drupal_set_message('No audio player for ' .$op, 'error');
      break;
  }
}

/**
 * Pull the file extension from a filename
 *
 * @param $vidfile
 *   string filename to get the filetype from.
 *
 * @return
 *   string value of file type or boolean FALSE on error
 */
function _video_field_get_filetype($vidfile) {
  if (strstr($vidfile, '.')) { //If file contains a "." then get the file extension after the ".
    $file_type = end(explode('.', $vidfile));
  }
  else {
    $file_type = FALSE;
  }

  return strtolower($file_type);
}


/*********************************************************************
 * Themeable functions for playing videos. They print a page with a player embedded.
 *********************************************************************/

/**
 * Play videos from in FLV Flash video format
 *
 * @param $node
 *   object with node information
 *
 * @return
 *   string of content to display
 */
function theme_audiosoxfield_play_mp3($element) {
  $audio = file_create_url($element['#item']['filepath']);
  $options['soundFile'] = check_url($audio);
  $url = base_path() . drupal_get_path('module', 'audio') .'/players/1pixelout.swf';
  $flashvars = audiofield_query_string_encode($options);

  $output = <<<EOT
<object type="application/x-shockwave-flash" data="$url" width="290" height="24" >
  <param name="movie" value="$url" />
  <param name="wmode" value="transparent" />
  <param name="menu" value="false" />
  <param name="quality" value="high" />
  <param name="FlashVars" value="$flashvars" />
  <embed src="$url" flashvars="$flashvars" width="290" height="24" />
</object>
EOT;

  return $output;
}

/**
 * Parse an array into a valid urlencoded query string.
 *
 * This function is a work-around for a drupal_urlencode issue in core.
 * See: http://drupal.org/node/158687 for details.
 *
 * @param $query
 *   The array to be processed e.g. $_GET.
 * @param $exclude
 *   The array filled with keys to be excluded. Use parent[child] to exclude
 *   nested items.
 * @param $parent
 *   Should not be passed, only used in recursive calls.
 * @return
 *   urlencoded string which can be appended to/as the URL query string.
 */
function audiofield_query_string_encode($query, $exclude = array(), $parent = '') {
  $params = array();

  foreach ($query as $key => $value) {
    $key = urlencode($key);
    if ($parent) {
      $key = $parent .'['. $key .']';
    }

    if (in_array($key, $exclude)) {
      continue;
    }

    if (is_array($value)) {
      $params[] = audiofield_query_string_encode($value, $exclude, $key);
    }
    else {
      $params[] = $key .'='. urlencode($value);
    }
  }

  return implode('&', $params);
}